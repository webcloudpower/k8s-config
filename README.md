# k8s-config
Configuration optimized for https://gitlab.com/webcloudpower/hetzner_cluster.

It supports:
* cert-manager (https)
* nginx-ingress
* velero (backup)
* cert-manager-webhook-hetzner (wildcard https)
* hetznerdns (automaticly configure dns on hetzner)
* kube-prometheus-stack for monitoring. 

## Getting started
Add to your terraform file the following:
```bash
module "k8s-config" {
  source = "git::https://gitlab.com/webcloudpower/k8s-config.git?ref=0.7.4"

  host                     = "http://localhost"
  cluster_ca_certificate   = "cluster_ca_certificate"
  client_certificate       = "client_certificate"
  client_key               = "client_key"

  dns_email                = "myemail@example.com"
  hcloud_dns_token         = "hcloud_dns_token"
  backup_access_key_id     = "aws or minio key id" 
  backup_secret_access_key = "aws or minio access key" 
  backup_s3Url             = "aws or minio url"
  backup_bucket            = "aws or minio bucket"
  backup_region            = "minio"
  schedule_name            = "schedule"
  base_domain              = "domain.com"
  dns_domains              = [{
    zone      = "domain.com",
    subdomain = null
  }]
}
```

## Testing
* Create a `terraform.tfvars` file with the following fields:
  ```
  hcloud_token = "<hcloud_token>"
  hcloud_dns_token = "<hcloud_dns_token>"
  base_domain = "mydomain.com"
  dns_domains = [{
    zone = "mydomain.com",
    subdomain = "qa"
  }]
  ```
* Make sure that the zone is configured on hetzner dns.
* Run:
  - `docker-compose run app bash -c "cd tests/cluster && terragrunt init && terragrunt apply"`
  - `docker-compose run app bash -c "cd tests && go test -timeout 99999s"`.
