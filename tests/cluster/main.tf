variable "hcloud_token" {}

module "cluster" {
  source = "git::https://gitlab.com/webcloudpower/hetzner_cluster.git?ref=0.8.3"

  hcloud_token = var.hcloud_token
}

output "host" {
  value     = module.cluster.host
  sensitive = true
}

output "cluster_ca_certificate" {
  value     = module.cluster.cluster_ca_certificate
  sensitive = true
}

output "client_certificate" {
  value     = module.cluster.client_certificate
  sensitive = true
}

output "client_key" {
  value     = module.cluster.client_key
  sensitive = true
}
