terraform {
  extra_arguments "custom_vars" {
    commands = [
      "apply",
      "plan",
      "import",
      "push",
      "refresh",
      "destroy"
    ]

    arguments = [
      "-var-file=../../terraform.tfvars"
    ]
  }
}

dependency "cluster" {
  config_path = "../cluster"
  skip_outputs = true
}
