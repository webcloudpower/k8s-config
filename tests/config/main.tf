variable "hcloud_dns_token" {}
variable "dns_domains" {
  type = list(
    object({
      zone      = string
      subdomain = string
    })
  )
}
variable "base_domain" {
  type = string
}

provider "kubernetes" {
  host                   = data.terraform_remote_state.cluster.outputs.host
  cluster_ca_certificate = data.terraform_remote_state.cluster.outputs.cluster_ca_certificate
  client_certificate     = data.terraform_remote_state.cluster.outputs.client_certificate
  client_key             = data.terraform_remote_state.cluster.outputs.client_key

  experiments {
    manifest_resource = true
  }
}

provider "helm" {
  kubernetes {
    host                   = data.terraform_remote_state.cluster.outputs.host
    cluster_ca_certificate = data.terraform_remote_state.cluster.outputs.cluster_ca_certificate
    client_certificate     = data.terraform_remote_state.cluster.outputs.client_certificate
    client_key             = data.terraform_remote_state.cluster.outputs.client_key
  }
}

data "terraform_remote_state" "cluster" {
  backend = "local"

  config = {
    path = "../cluster/terraform.tfstate"
  }
}

resource "kubernetes_namespace" "minio" {
  metadata {
    name = "minio"
  }
}

resource "helm_release" "minio" {
  depends_on = [kubernetes_namespace.minio]
  name       = "minio"
  repository = "https://charts.bitnami.com/bitnami"
  namespace  = "minio"

  version = "11.10.24"

  chart = "minio"

  set {
    name  = "mode"
    value = "standalone"
  }

  set {
    name  = "auth.rootUser"
    value = "root"
  }

  set {
    name  = "auth.rootPassword"
    value = "rootpass123"
  }

  set {
    name  = "service.type"
    value = "LoadBalancer"
  }

  set {
    name  = "defaultBuckets"
    value = "backup"
  }
}

data "kubernetes_service" "minio" {
  depends_on = [helm_release.minio]
  metadata {
    name      = "minio"
    namespace = "minio"
  }
}

module "k8s_config" {
  source = "../../"

  host                   = data.terraform_remote_state.cluster.outputs.host
  cluster_ca_certificate = data.terraform_remote_state.cluster.outputs.cluster_ca_certificate
  client_certificate     = data.terraform_remote_state.cluster.outputs.client_certificate
  client_key             = data.terraform_remote_state.cluster.outputs.client_key

  dns_email                = "aronwolf90@gmail.com"
  hcloud_dns_token         = var.hcloud_dns_token
  backup_access_key_id     = "root"
  backup_secret_access_key = "rootpass123"
  backup_s3Url             = "http://${data.kubernetes_service.minio.status.0.load_balancer.0.ingress.0.ip}:9000"
  backup_bucket            = "backup"
  backup_region            = "minio"
  dns_domains              = var.dns_domains
  base_domain              = var.base_domain
  schedule_name            = "schedule"
}

output "dns_domains" {
  value = var.dns_domains
}

output "basic_auth" {
  value     = module.k8s_config.basic_auth
  sensitive = true
}

output "prometheus_domain" {
  value = module.k8s_config.prometheus_domain
}
