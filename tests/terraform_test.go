package tests

import (
	"context"
	"fmt"
	"strings"
	"testing"
	"time"

	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	metricsv "k8s.io/metrics/pkg/client/clientset/versioned"

	"github.com/go-resty/resty/v2"
	"github.com/gruntwork-io/terratest/modules/terraform"
	"github.com/stretchr/testify/assert"

	"encoding/json"
	"k8s.io/apimachinery/pkg/api/meta"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/runtime/serializer/yaml"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/discovery"
	"k8s.io/client-go/discovery/cached/memory"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/restmapper"

	"strconv"
)

func CreateClientset(host string, token string) *kubernetes.Clientset {
	config := &rest.Config{
		BearerToken: token,
		Host:        host,
		TLSClientConfig: rest.TLSClientConfig{
			Insecure: true,
		},
	}
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(err.Error())
	}
	return clientset
}

func CreateConfig(host string, clusterCaCertificate string, clientCertificate string, clientKey string) *rest.Config {
	return &rest.Config{
		Host: host,
		TLSClientConfig: rest.TLSClientConfig{
			CAData:   []byte(clusterCaCertificate),
			CertData: []byte(clientCertificate),
			KeyData:  []byte(clientKey),
		},
	}
}

func applyYAML(cfg *rest.Config, deploymentYAML string) {
	ctx := context.TODO()

	decUnstructured := yaml.NewDecodingSerializer(unstructured.UnstructuredJSONScheme)

	// 1. Prepare a RESTMapper to find GVR
	dc, err := discovery.NewDiscoveryClientForConfig(cfg)
	if err != nil {
		panic(err.Error())
	}
	mapper := restmapper.NewDeferredDiscoveryRESTMapper(memory.NewMemCacheClient(dc))

	// 2. Prepare the dynamic client
	dyn, err := dynamic.NewForConfig(cfg)
	if err != nil {
		panic(err.Error())
	}

	// 3. Decode YAML manifest into unstructured.Unstructured
	obj := &unstructured.Unstructured{}
	_, gvk, err := decUnstructured.Decode([]byte(deploymentYAML), nil, obj)
	if err != nil {
		panic(err.Error())
	}

	// 4. Find GVR
	mapping, err := mapper.RESTMapping(gvk.GroupKind(), gvk.Version)
	if err != nil {
		panic(err.Error())
	}

	// 5. Obtain REST interface for the GVR
	var dr dynamic.ResourceInterface
	if mapping.Scope.Name() == meta.RESTScopeNameNamespace {
		// namespaced resources should specify the namespace
		dr = dyn.Resource(mapping.Resource).Namespace(obj.GetNamespace())
	} else {
		// for cluster-wide resources
		dr = dyn.Resource(mapping.Resource)
	}

	// 6. Marshal object into JSON
	data, err := json.Marshal(obj)
	if err != nil {
		panic(err.Error())
	}

	// 7. Create or Update the object with SSA
	//     types.ApplyPatchType indicates SSA.
	//     FieldManager specifies the field owner ID.
	_, err = dr.Patch(ctx, obj.GetName(), types.ApplyPatchType, data, metav1.PatchOptions{
		FieldManager: "sample-controller",
	})

	if err != nil {
		panic(err.Error())
	}
}

func GetResource(config *rest.Config, group string, version string, resource string, namespace string) []unstructured.Unstructured {
	ctx := context.TODO()
	dynamic := dynamic.NewForConfigOrDie(config)
	resourceId := schema.GroupVersionResource{
		Group:    group,
		Version:  version,
		Resource: resource,
	}
	list, err := dynamic.Resource(resourceId).Namespace(namespace).
		List(ctx, metav1.ListOptions{})

	if err != nil {
		panic(err.Error())
	}

	return list.Items
}

func DeleteCollection(config *rest.Config, group string, version string, resource string, namespace string) {
	ctx := context.TODO()
	dynamic := dynamic.NewForConfigOrDie(config)
	resourceId := schema.GroupVersionResource{
		Group:    group,
		Version:  version,
		Resource: resource,
	}
	err := dynamic.Resource(resourceId).Namespace(namespace).
		DeleteCollection(ctx, metav1.DeleteOptions{}, metav1.ListOptions{})

	if err != nil {
		panic(err.Error())
	}
}

func ApplyNamespace(config *rest.Config) {
	const namespaceYAML = `
  apiVersion: v1
  kind: Namespace
  metadata:
    name: test
  `
	applyYAML(config, namespaceYAML)
}

func ApplyDeploymentWithInit(config *rest.Config) {
	fmt.Printf("ApplyDeploymentWithInit\n")

	const deploymentYAML = `
  apiVersion: apps/v1
  kind: Deployment
  metadata:
    name: demo-deployment
    namespace: test
  spec:
  apiVersion: apps/v1
  kind: StatefulSet
  metadata:
    name: demo-deployment
    namespace: test
  spec:
    serviceName: "demo-deployment"
    replicas: 1
    selector:
      matchLabels:
        app: demo
    template:
      metadata:
        labels:
          app: demo
        annotations:
          backup.velero.io/backup-volumes: test
      spec:
        containers:
        - name: web
          image: registry.k8s.io/nginx-slim:0.8
          command: [ "sh", "-c", "echo {} > /usr/share/nginx/html/test/ok.json; sleep 1000000" ]
          ports:
          - containerPort: 80
            name: web
          volumeMounts:
          - name: test
            mountPath: /usr/share/nginx/html/test
    volumeClaimTemplates:
    - metadata:
        name: test
      spec:
        accessModes: [ "ReadWriteOnce" ]
        resources:
          requests:
            storage: 1Gi
  `
	applyYAML(config, deploymentYAML)
}

func ApplyDeployment(config *rest.Config) {
	fmt.Printf("ApplyDeployment\n")

	const deploymentYAML = `
  apiVersion: apps/v1
  kind: StatefulSet
  metadata:
    name: demo-deployment
    namespace: test
  spec:
    serviceName: "demo-deployment"
    replicas: 1
    selector:
      matchLabels:
        app: demo
    template:
      metadata:
        labels:
          app: demo
        annotations:
          backup.velero.io/backup-volumes: test
      spec:
        containers:
        - name: web
          image: registry.k8s.io/nginx-slim:0.8
          ports:
          - containerPort: 80
            name: web
          volumeMounts:
          - name: test
            mountPath: /usr/share/nginx/html/test
    volumeClaimTemplates:
    - metadata:
        name: test
      spec:
        accessModes: [ "ReadWriteOnce" ]
        resources:
          requests:
            storage: 1Gi
  `
	applyYAML(config, deploymentYAML)
}

func ApplyService(config *rest.Config) {
	fmt.Printf("ApplyService\n")

	const serviceYAML = `
  apiVersion: v1
  kind: Service
  metadata:
    name: test
    namespace: test
  spec:
    ports:
      - port: 80
        targetPort: 80
        protocol: TCP
    selector:
      app: demo
  `
	applyYAML(config, serviceYAML)
}

func ApplyIngress(config *rest.Config, domain string) {
	fmt.Printf("ApplyIngress\n")

	ingressYAML := fmt.Sprintf(`
  apiVersion: extensions/v1beta1
  kind: Ingress
  metadata:
    name: test
    namespace: test
    annotations:
      cert-manager.io/cluster-issuer: letsencrypt-production
      kubernetes.io/ingress.class: nginx
      kubernetes.io/tls-acme: "true"
  spec:
    rules:
    - host: "*.%s"
      http:
        paths:
        - backend:
            serviceName: test
            servicePort: 80
          path: /
          pathType: ImplementationSpecific
    tls:
    - hosts:
      - "*.%s"
      secretName: test-tls
  `, domain, domain)
	applyYAML(config, ingressYAML)
}

func ApplyBackup(config *rest.Config, timestamp string) {
	fmt.Printf("ApplyBackup\n")

	backupYAML := fmt.Sprintf(`
  apiVersion: velero.io/v1
  kind: Backup
  metadata:
    name: test-%s
    namespace: velero
  spec:
    includedNamespaces: [test]
    includedResources: ['*']
  `, timestamp)
	applyYAML(config, backupYAML)
}

func DeleteNamespace(config *rest.Config) {
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(err.Error())
	}

	clientset.CoreV1().Namespaces().Delete(context.TODO(), "test", metav1.DeleteOptions{})
	count := 0
	for {
		_, err := clientset.CoreV1().Namespaces().Get(context.TODO(), "test", metav1.GetOptions{})
		if err != nil {
			break
		}
		count += 1

		if count > 420 {
			panic("Namespace can not be deleted.")
		}
		time.Sleep(time.Second)
	}
}

func ApplyRestore(config *rest.Config, timestamp string) {
	fmt.Printf("ApplyRestore\n")

	restoreYAML := fmt.Sprintf(`
  apiVersion: velero.io/v1
  kind: Restore
  metadata:
    name: test-%s
    namespace: velero
  spec:
    backupName: test-%s
  `, timestamp, timestamp)
	applyYAML(config, restoreYAML)
}

func CheckDeploymentWorking(working bool, domain string) {
	fmt.Printf("CheckDeploymentWorking: %t\n", working)
	client := resty.New()

	count := 0
	for {
		resp, err := client.R().
			EnableTrace().
			Get(fmt.Sprintf("https://test.%s/test/ok.json", domain))
		if working && (err == nil && resp.StatusCode() == 200) {
			break
		} else if !working && (err != nil || resp.StatusCode() != 200) {
			break
		}

		count += 1

		if count > 180 {
			panic("Check failed")
		}
		time.Sleep(time.Second)
	}
}

func CreateTimestamp() string {
	now := time.Now()
	seconds := now.Unix()
	return strconv.FormatInt(seconds, 10)
}

func setup(t *testing.T) (*terraform.Options, *terraform.Options, *rest.Config, string) {
	terraformOptionsCluster := terraform.WithDefaultRetryableErrors(t, &terraform.Options{
		TerraformDir: "./cluster/",
		VarFiles:     []string{"../../terraform.tfvars"},
	})
	terraformOptionsConfig := terraform.WithDefaultRetryableErrors(t, &terraform.Options{
		TerraformDir: "./config/",
		VarFiles:     []string{"../../terraform.tfvars"},
	})
	terraform.InitAndApply(t, terraformOptionsConfig)

	outputHost := terraform.Output(t, terraformOptionsCluster, "host")
	clusterCaCertificate := terraform.Output(t, terraformOptionsCluster, "cluster_ca_certificate")
	clientCertificate := terraform.Output(t, terraformOptionsCluster, "client_certificate")
	clientKey := terraform.Output(t, terraformOptionsCluster, "client_key")
	config := CreateConfig(outputHost, clusterCaCertificate, clientCertificate, clientKey)

	dnsDomains := terraform.OutputListOfObjects(t, terraformOptionsConfig, "dns_domains")
	subdomain, _, _ := unstructured.NestedString(dnsDomains[0], "subdomain")
	zone, _, _ := unstructured.NestedString(dnsDomains[0], "zone")
	domain := strings.Join([]string{subdomain, zone}, ".")

	return terraformOptionsCluster, terraformOptionsConfig, config, domain
}

type RulesResult struct {
	Data struct {
		Groups []struct {
			Name  string `json:"name"`
			Rules []struct {
				State string `json:"state"`
			} `json:"rules"`
		} `json:"groups"`
	} `json:"data"`
}

func GetPromethousRules(prometheusDomain string, basic_auth map[string]string) RulesResult {
	client := resty.New()
	client.SetBasicAuth(basic_auth["user"], basic_auth["password"])
	count := 0
	var resp *resty.Response
	var err error
	for {
		resp, err = client.R().
			EnableTrace().
			Get(fmt.Sprintf("https://%s/api/v1/rules?type=alert", prometheusDomain))
		if err == nil && resp.StatusCode() == 200 {
			break
		} else if err != nil || resp.StatusCode() != 200 {
			break
		}

		count += 1

		if count > 180 {
			panic("Prometheus not working")
		}
		time.Sleep(time.Second)
	}

	var parsedResult RulesResult
	json.Unmarshal(resp.Body(), &parsedResult)
	return parsedResult
}

func TestTerraform(t *testing.T) {
	_, _, config, domain := setup(t)

	timestamp := CreateTimestamp()

	DeleteNamespace(config)
	ApplyNamespace(config)
	ApplyDeploymentWithInit(config)
	ApplyService(config)
	ApplyIngress(config, domain)
	time.Sleep(time.Second * 20)
	ApplyDeployment(config)
	CheckDeploymentWorking(true, domain)
	ApplyBackup(config, timestamp)
	time.Sleep(time.Second * 10)
	DeleteNamespace(config)
	CheckDeploymentWorking(false, domain)
	ApplyRestore(config, timestamp)
	CheckDeploymentWorking(true, domain)
}

func TestMatricsServer(t *testing.T) {
	_, _, config, _ := setup(t)

	clientset, err := metricsv.NewForConfig(config)
	if err != nil {
		panic(err.Error())
	}

	_, err = clientset.MetricsV1beta1().PodMetricses("").List(context.TODO(), metav1.ListOptions{})
	if err != nil {
		panic(err)
	}
}

func TestBackupSchedule(t *testing.T) {
	_, _, config, _ := setup(t)

	schedules := GetResource(config, "velero.io", "v1", "schedules", "velero")
	schedule, _, _ := unstructured.NestedString(schedules[0].Object, "spec", "schedule")
	excludedNamespaces, _, _ := unstructured.NestedStringSlice(schedules[0].Object, "spec", "template", "excludedNamespaces")
	assert.Equal(t, len(schedules), 1)
	assert.Equal(t, schedule, "0 0 * * *")
	assert.Equal(t, excludedNamespaces, []string{
		"cert-manager", "kube-node-lease",
		"kube-public", "kube-system",
		"metrics-server", "minio",
		"nginx-ingress", "velero",
	})
}

func TestKubePrometheusStack(t *testing.T) {
	_, terraformOptionsConfig, _, _ := setup(t)

	prometheusDomain := terraform.Output(t, terraformOptionsConfig, "prometheus_domain")
	basic_auth := terraform.OutputMap(t, terraformOptionsConfig, "basic_auth")
	parsedResult := GetPromethousRules(prometheusDomain, basic_auth)
	totalRulesCount := 0
	inactiveRulesCount := 0
	for _, group := range parsedResult.Data.Groups {
		for _, rule := range group.Rules {
			totalRulesCount++
			if rule.State == "inactive" {
				inactiveRulesCount++
			}
		}
	}
	assert.GreaterOrEqual(t, inactiveRulesCount, 122)
	assert.Equal(t, totalRulesCount, 132)

	hasVeleroGroup := false
	for _, group := range parsedResult.Data.Groups {
		if group.Name == "velero" {
			hasVeleroGroup = true
		}
	}
	assert.Equal(t, hasVeleroGroup, true)
}
