terraform {
  required_version = ">= 0.13"

  required_providers {
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = ">= 1.7.0"
    }
    hetznerdns = {
      source  = "timohirt/hetznerdns"
      version = ">= 1.2.0"
    }
    htpasswd = {
      source  = "loafoe/htpasswd"
      version = ">= 1.0.4"
    }
  }
}

provider "kubernetes" {
  host                   = var.host
  cluster_ca_certificate = var.cluster_ca_certificate
  client_certificate     = var.client_certificate
  client_key             = var.client_key

  experiments {
    manifest_resource = true
  }
}

provider "helm" {
  kubernetes {
    host                   = var.host
    cluster_ca_certificate = var.cluster_ca_certificate
    client_certificate     = var.client_certificate
    client_key             = var.client_key
  }
}

provider "hetznerdns" {
  apitoken = var.hcloud_dns_token
}

provider "htpasswd" {
}


variable "dns_email" {
  type = string

  nullable = false
}
variable "dns_domains" {
  type = list(
    object({
      zone      = string
      subdomain = string
    })
  )

  nullable = false
}
variable "host" {
  type = string

  nullable = false
}
variable "cluster_ca_certificate" {
  type = string

  nullable = false
}
variable "client_certificate" {
  type = string

  nullable = false
}
variable "client_key" {
  type = string

  nullable = false
}
variable "backup_access_key_id" {
  type = string

  nullable = false
}
variable "backup_secret_access_key" {
  type = string

  nullable = false
}
variable "backup_s3Url" {
  type = string

  nullable = false
}
variable "backup_bucket" {
  type = string

  nullable = false
}
variable "hcloud_dns_token" {
  type = string

  nullable = false
}
variable "backup_region" {
  type = string

  nullable = false
}
variable "schedule_name" {
  type = string

  nullable = false
}
variable "base_domain" {
  type = string

  nullable = false
}

locals {
  dns_domains       = { for domain in var.dns_domains : join(".", compact([domain.subdomain, domain.zone])) => { subdomain = domain.subdomain, zone = domain.zone } }
  prometheus_domain = "prometheus.${var.base_domain}"
  cluster_issuer = {
    apiVersion : "cert-manager.io/v1",
    kind : "ClusterIssuer",
    metadata : {
      name : "letsencrypt-production"
    },
    spec : {
      acme : {
        email : var.dns_email,
        privateKeySecretRef : {
          name : "letsencrypt-sec-production",
        },
        server : "https://acme-v02.api.letsencrypt.org/directory",
        solvers : concat([
          {
            http01 : {
              ingress : {
                class : "nginx"
              }
            }
          }],
          [
            for dns_name, domain in local.dns_domains :
            {
              dns01 : {
                webhook : {
                  groupName : "acme.domain.tld",
                  solverName : "hetzner",
                  config : {
                    secretName : "hetzner-secret",
                    apiUrl : "https://dns.hetzner.com/api/v1"
                  }
                }
              },
              selector : {
                dnsZones : [
                  "${dns_name}"
                ]
              }
            }
          ]
        )
      }
    }
  }
  velero_schedule = {
    apiVersion : "velero.io/v1",
    kind : "Schedule",
    metadata : {
      name : "${var.schedule_name}"
    },
    spec : {
      schedule : "0 0 * * *",
      template : {
        excludedNamespaces : [
          "cert-manager",
          "kube-node-lease",
          "kube-public",
          "kube-system",
          "minio",
          "nginx-ingress",
          "velero"
        ],
        excludedResources : [
          "storageclasses.storage.k8s.io",
          "events.events.k8s.io",
          "events"
        ]
      }
    }
  }
}

resource "kubernetes_namespace" "cert_manager" {
  metadata {
    name = "cert-manager"
  }
}

resource "helm_release" "cert_manager" {
  depends_on = [kubernetes_namespace.cert_manager]

  name       = "cert-manager"
  repository = "https://charts.jetstack.io"
  namespace  = "cert-manager"

  chart   = "cert-manager"
  version = "v1.5.3"

  set {
    name  = "ingressShim.defaultIssuerName"
    value = "letsencrypt-production"
  }
  set {
    name  = "ingressShim.defaultIssuerKind"
    value = "ClusterIssuer"
  }
  set {
    name  = "installCRDs"
    value = "true"
  }
}

resource "kubernetes_secret" "hetzner_secret" {
  depends_on = [helm_release.cert_manager]

  metadata {
    name      = "hetzner-secret"
    namespace = "cert-manager"
  }
  type = "Opaque"
  data = {
    api-key = var.hcloud_dns_token
  }
}

resource "helm_release" "cert_manager_webhook_hetzner" {
  depends_on = [kubernetes_secret.hetzner_secret]

  name       = "cert-manager-webhook-hetzner"
  repository = "https://vadimkim.github.io/cert-manager-webhook-hetzner"
  namespace  = "cert-manager"
  version    = "1.2.2"

  chart = "cert-manager-webhook-hetzner"

  set {
    name  = "groupName"
    value = "acme.domain.tld"
  }
}

resource "null_resource" "letsencrypt_production" {
  depends_on = [helm_release.cert_manager]

  triggers = {
    yaml = yamlencode(local.cluster_issuer)
  }

  provisioner "local-exec" {
    command = <<-EOT
cat <<EOF > ${path.module}/tmp/cluster_ca_certificate
${var.cluster_ca_certificate}
EOF
EOT
  }

  provisioner "local-exec" {
    command = <<-EOT
cat <<EOF > ${path.module}/tmp/client_certificate
${var.client_certificate}
EOF
EOT
  }

  provisioner "local-exec" {
    command = <<-EOT
cat <<EOF > ${path.module}/tmp/client_key
${var.client_key}
EOF
EOT
  }

  provisioner "local-exec" {
    command = <<EOF
KUBECONFIG=/dev/null kubectl --server=${var.host} --certificate-authority=${path.module}/tmp/cluster_ca_certificate --client-certificate=${path.module}/tmp/client_certificate --client-key=${path.module}/tmp/client_key -n cert-manager apply -f - <<YAML
${yamlencode(local.cluster_issuer)}
YAML
EOF
  }
}

data "kubernetes_service" "nginx_ingress" {
  depends_on = [helm_release.nginx_ingress]
  metadata {
    name      = "nginx-ingress-ingress-nginx-controller"
    namespace = "nginx-ingress"
  }
}

data "hetznerdns_zone" "zone" {
  for_each = local.dns_domains

  name = each.value.zone
}

resource "hetznerdns_record" "main" {
  for_each = local.dns_domains

  zone_id = data.hetznerdns_zone.zone[each.key].id
  name    = "${each.value.subdomain}" == "" ? "@" : each.value.subdomain
  value   = data.kubernetes_service.nginx_ingress.status.0.load_balancer.0.ingress.0.ip
  type    = "A"
  ttl     = 300
}

resource "hetznerdns_record" "other" {
  for_each = local.dns_domains

  zone_id = data.hetznerdns_zone.zone[each.key].id
  name    = "${each.value.subdomain}" == "" ? "*" : "*.${each.value.subdomain}"
  value   = data.kubernetes_service.nginx_ingress.status.0.load_balancer.0.ingress.0.ip
  type    = "A"
  ttl     = 300
}

resource "kubernetes_namespace" "kube_prometheus_stack" {
  metadata {
    name = "kube-prometheus-stack"
  }
}

resource "random_password" "password" {
  length = 30
}

resource "kubernetes_secret" "basic-auth" {
  depends_on = [kubernetes_namespace.kube_prometheus_stack]
  type       = "Opaque"

  metadata {
    name      = "basic-auth"
    namespace = "kube-prometheus-stack"
  }

  data = {
    auth = "admin:${random_password.password.bcrypt_hash}"
  }
}

resource "helm_release" "kube-prometheus-stack" {
  depends_on = [kubernetes_namespace.kube_prometheus_stack]

  name       = "kube-prometheus-stack"
  repository = "https://prometheus-community.github.io/helm-charts"
  namespace  = "kube-prometheus-stack"

  chart = "kube-prometheus-stack"

  values = [
    <<-EOF
    alertmanager:
      ingress:
        enabled: true
        hosts:
          - alertmanager.${var.base_domain}
        annotations:
          nginx.ingress.kubernetes.io/auth-type: basic
          nginx.ingress.kubernetes.io/auth-secret: basic-auth
          cert-manager.io/cluster-issuer: letsencrypt-production
        tls:
          - secretName: alertmanager-tls
            hosts:
              - alertmanager.${var.base_domain}
    grafana:
      enabled: false

    prometheus:
      ingress:
        enabled: true
        hosts:
          - ${local.prometheus_domain}
        annotations:
          nginx.ingress.kubernetes.io/auth-type: basic
          nginx.ingress.kubernetes.io/auth-secret: basic-auth
          cert-manager.io/cluster-issuer: letsencrypt-production
        tls:
          - secretName: prometheus-tls
            hosts:
              - ${local.prometheus_domain}

    kubeControllerManager:
      service:
        port: 10257
        targetPort: 10257
      serviceMonitor:
        https: true
        insecureSkipVerify: true
    kubeScheduler:
      service:
        port: 10259
        targetPort: 10259
      serviceMonitor:
        https: true
        insecureSkipVerify: true
    kubeEtcd:
      enabled: false
    kubeProxy:
      enabled: false
    EOF
  ]
}

data "http" "nginx_ingress" {
  url = "https://raw.githubusercontent.com/jetstack/cert-manager/release-0.11/deploy/manifests/00-crds.yaml"
}

resource "kubernetes_namespace" "nginx_ingress" {
  depends_on = [helm_release.kube-prometheus-stack]
  metadata {
    name = "nginx-ingress"
  }
}

resource "helm_release" "nginx_ingress" {
  depends_on = [kubernetes_namespace.nginx_ingress]
  name       = "nginx-ingress"
  repository = "https://kubernetes.github.io/ingress-nginx"
  namespace  = "nginx-ingress"

  chart   = "ingress-nginx"
  version = "3.26.0"

  values = [
    "${file("${path.module}/nginx_ingress_values.yaml")}"
  ]
}


resource "kubernetes_namespace" "velero" {
  depends_on = [helm_release.kube-prometheus-stack]

  metadata {
    name = "velero"
  }
}

resource "helm_release" "velero" {
  depends_on = [kubernetes_namespace.velero]

  name       = "velero"
  repository = "https://vmware-tanzu.github.io/helm-charts"
  namespace  = "velero"

  chart   = "velero"
  version = "2.32.6"

  values = [
    "${file("${path.module}/velero-values.yaml")}"
  ]

  set {
    name  = "credentials.secretContents.cloud"
    value = <<EOF
[default]
aws_access_key_id = ${var.backup_access_key_id}
aws_secret_access_key = ${var.backup_secret_access_key}
EOF
  }

  set {
    name  = "configuration.backupStorageLocation.config.publicUrl"
    value = var.backup_s3Url
  }
  set {
    name  = "configuration.backupStorageLocation.config.s3Url"
    value = var.backup_s3Url
  }
  set {
    name  = "configuration.volumeSnapshotLocation.config.region"
    value = var.backup_region
  }
  set {
    name  = "configuration.backupStorageLocation.bucket"
    value = var.backup_bucket
  }

  set {
    name  = "configuration.volumeSnapshotLocation.config.publicUrl"
    value = var.backup_s3Url
  }
  set {
    name  = "configuration.volumeSnapshotLocation.config.region"
    value = var.backup_region
  }
  set {
    name  = "configuration.volumeSnapshotLocation.config.s3Url"
    value = var.backup_s3Url
  }
  set {
    name  = "configuration.volumeSnapshotLocation.bucket"
    value = var.backup_bucket
  }
}

resource "null_resource" "velero_schedule" {
  depends_on = [helm_release.velero]

  triggers = {
    yaml = yamlencode(local.velero_schedule)
  }

  provisioner "local-exec" {
    command = <<-EOT
cat <<EOF > ${path.module}/tmp/cluster_ca_certificate
${var.cluster_ca_certificate}
EOF
EOT
  }

  provisioner "local-exec" {
    command = <<-EOT
cat <<EOF > ${path.module}/tmp/client_certificate
${var.client_certificate}
EOF
EOT
  }

  provisioner "local-exec" {
    command = <<-EOT
cat <<EOF > ${path.module}/tmp/client_key
${var.client_key}
EOF
EOT
  }


  provisioner "local-exec" {
    command = <<EOF
KUBECONFIG=/dev/null kubectl --server=${var.host} --certificate-authority=${path.module}/tmp/cluster_ca_certificate --client-certificate=${path.module}/tmp/client_certificate --client-key=${path.module}/tmp/client_key -n velero apply -f - <<YAML
${yamlencode(local.velero_schedule)}
YAML
EOF
  }
}


output "basic_auth" {
  value = {
    user     = "admin"
    password = random_password.password.result
  }
  sensitive = true
}

output "prometheus_domain" {
  value = local.prometheus_domain
}
